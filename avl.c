/*
 * This file is part of dscomp-avl.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

/* assert() */
#include <assert.h>

/* true */
#if HAVE_STDBOOL_H
# include <stdbool.h>
#endif /* HAVE_STDBOOL_H */

/* SIZE_MAX */
#include <stdint.h>

/* free(), malloc() */
#include <stdlib.h>

/* fprintf(), fputs(), perror(), stderr */
#include <stdio.h>

#include "avl.h"

#if RECYCLE
# define avl_node_remove(avl, node) avl_node_recycle(avl, node)
#else /* !RECYCLE */
# define avl_node_remove(avl, node) avl_node_destroy(node)
#endif /* !RECYCLE */

static inline void __attribute__((nonnull))
avl_node_init ( avl_node_t * const node,
                const int key,
                const int value )
{
	node->key = key;
	node->value = value;
	node->factor = 0;
	node->left = NULL;
	node->right = NULL;

	/* node->parent does not need to be set. It will be set upon
	 * insertion. */

	return;
}

static inline avl_node_t * __attribute__((nonnull))
avl_node_new ( avl_t * const avl,
               const int key,
               const int value )
{
#if RECYCLE
	if ( avl->recycled != NULL ) {
		avl_node_t * const node = avl->recycled;
		avl->recycled = node->right;
		avl_node_init(node, key, value);
		return node;
	}
#endif /* RECYCLE */

	avl_node_t * const node = malloc(sizeof(avl_node_t));
	if ( node == NULL ) {
		perror("malloc");
		return NULL;
	}
	avl_node_init(node, key, value);
	return node;
}

static void __attribute__((nonnull))
avl_node_destroy ( avl_node_t * const node )
{
	free(node);
	return;
}

#if RECYCLE
static void __attribute__((nonnull))
avl_node_recycle ( avl_t * const avl,
                   avl_node_t * const node )
{
	node->right = avl->recycled;
	avl->recycled = node;
	return;
}
#endif /* RECYCLE */

static void __attribute__((nonnull))
avl_node_remove_recursive ( avl_t * const avl,
                            avl_node_t * const node )
{
	if ( node->left != NULL ) {
		avl_node_remove_recursive(avl, node->left);
	}
	if ( node->right != NULL ) {
		avl_node_remove_recursive(avl, node->right);
	}
	avl_node_remove(avl, node);
	return;
}

avl_t *
avl_new ( void )
{
	avl_t * const avl = malloc(sizeof(avl_t));
	if ( avl == NULL ) {
		perror("malloc");
		return NULL;
	}

	avl->root = NULL;

#if RECYCLE
	avl->recycled = NULL;
#endif /* RECYCLE */

#if STACK
	avl->stack = NULL;
	/* There is no need to set stack_len here as it is set by
	 * avl_stack_clear() before the stack is used. */
	/* avl->stack_len = 0; */
	avl->stack_size = 0;
#endif /* STACK */

	return avl;
}

void __attribute__((nonnull))
avl_destroy ( avl_t * const avl )
{
	avl_clear(avl);

#if RECYCLE
	avl_node_t * node = avl->recycled;
	while ( node != NULL ) {
		avl_node_t * const next = node->right;
		avl_node_destroy(node);
		node = next;
	}
#endif /* RECYCLE */

#if STACK
	free(avl->stack);
#endif /* STACK */

	free(avl);
	return;
}

void __attribute__((nonnull))
avl_clear ( avl_t * const avl )
{
	if ( avl->root != NULL ) {
		avl_node_remove_recursive(avl, avl->root);
		avl->root = NULL;
	}
	return;
}

static avl_node_t * __attribute__((nonnull, warn_unused_result))
left_balance ( avl_node_t * const node )
{
	/* Require that the node is unbalanced. The check for -1 is needed
	 * because the recursive implementation will not decrement factor
	 * to -2 before rotating. */
	assert(node->factor == -1 || node->factor == -2);

	avl_node_t * const left = node->left;
	assert(left->factor == -1 || left->factor == 1);

	avl_node_t * const right = left->right;
	if ( left->factor == 1 ) {
		assert(right->factor >= -1 && right->factor <= 1);

		/* Left-right rotation. */
		node->left = right->right;
		right->right = node;
		left->right = right->left;
		right->left = left;

		if ( right->factor == -1 ) {
			node->factor = 1;
			left->factor = 0;
		} else if ( right->factor == 0 ) {
			node->factor = 0;
			left->factor = 0;
		} else /* if ( right->factor == 1 ) */ {
			node->factor = 0;
			left->factor = -1;
		}
		right->factor = 0;

#if PARENT
		right->parent = node->parent;
		node->parent = right;
		left->parent = right;
		if ( node->left != NULL ) {
			node->left->parent = node;
		}
		if ( left->right != NULL ) {
			left->right->parent = left;
		}
#endif /* PARENT */

		return right;
	} else /* if ( left->factor == -1 ) */ {
		/* Left-left rotation. */
		node->left = right;
		left->right = node;

		node->factor = 0;
		left->factor = 0;

#if PARENT
		left->parent = node->parent;
		node->parent = left;
		if ( right != NULL ) {
			right->parent = node;
		}
#endif /* PARENT */

		return left;
	}
}

static avl_node_t * __attribute__((nonnull, warn_unused_result))
right_balance ( avl_node_t * const node )
{
	/* Require that the node is unbalanced. The check for 1 is needed
	 * because the recursive implementation will not increment factor
	 * to 2 before rotating. */
	assert(node->factor == 1 || node->factor == 2);

	avl_node_t * const right = node->right;
	assert(right->factor == -1 || right->factor == 1);

	avl_node_t * const left = right->left;
	if ( right->factor == 1 ) {
		/* Right-right rotation. */
		node->right = left;
		right->left = node;

		node->factor = 0;
		right->factor = 0;

#if PARENT
		right->parent = node->parent;
		node->parent = right;
		if ( left != NULL ) {
			left->parent = node;
		}
#endif /* PARENT */

		return right;
	} else /* if ( right->factor == -1 ) */ {
		assert(left->factor >= -1 && left->factor <= 1);

		/* Right-left rotation. */
		node->right = left->left;
		left->left = node;
		right->left = left->right;
		left->right = right;

		if ( left->factor == -1 ) {
			node->factor = 0;
			right->factor = 1;
		} else if ( left->factor == 0 ) {
			node->factor = 0;
			right->factor = 0;
		} else /* if ( left->factor == 1 ) */ {
			node->factor = -1;
			right->factor = 0;
		}
		left->factor = 0;

#if PARENT
		left->parent = node->parent;
		node->parent = left;
		right->parent = left;
		if ( node->right != NULL ) {
			node->right->parent = node;
		}
		if ( right->left != NULL ) {
			right->left->parent = right;
		}
#endif /* PARENT */

		return left;
	}
}

#if PARENT
avl_error_t __attribute__((nonnull))
avl_insert ( avl_t * const avl,
             const int key,
             const int value )
{
	if ( avl->root == NULL ) {
		avl->root = avl_node_new(avl, key, value);
		if ( avl->root == NULL ) {
			return AVL_NOMEM;
		}
		avl->root->parent = NULL;
		return AVL_OK;
	}

	avl_node_t * node = avl->root;
	while ( true ) {
		 if ( key < node->key ) {
			if ( node->left != NULL ) {
				node = node->left;
			} else /* if ( node->left == NULL ) */ {
				avl_node_t * const new = avl_node_new(avl, key, value);
				if ( new == NULL ) {
					return AVL_NOMEM;
				}

				node->left = new;
				new->parent = node;
				break;
			}
		} else if ( key > node->key ) {
			if ( node->right != NULL ) {
				node = node->right;
			} else /* if ( node->right == NULL ) */ {
				avl_node_t * const new = avl_node_new(avl, key, value);
				if ( new == NULL ) {
					return AVL_NOMEM;
				}

				node->right = new;
				new->parent = node;
				break;
			}
		} else /* if ( key == node->key ) */ {
			return AVL_EXISTS;
		}
	}

	while ( node != NULL ) {
		if ( key < node->key ) {
			node->factor--;
			if ( node->factor == 0 ) {
				return AVL_OK;
			} else if ( node->factor == -2 ) {
				avl_node_t * const parent = node->parent;
				if ( parent != NULL ) {
					assert(node == parent->left ||
					       node == parent->right);
					if ( node == parent->left ) {
						parent->left = left_balance(node);
					} else /* if ( node == parent->right ) */ {
						parent->right = left_balance(node);
					}
				} else /* if ( parent == NULL ) */ {
					avl->root = left_balance(node);
				}
				return AVL_OK;
			}
		} else /* if ( key > node->key ) */ {
			node->factor++;
			if ( node->factor == 0 ) {
				return AVL_OK;
			} else if ( node->factor == 2 ) {
				avl_node_t * const parent = node->parent;
				if ( parent != NULL ) {
					assert(node == parent->left ||
					       node == parent->right);
					if ( node == parent->left ) {
						parent->left = right_balance(node);
					} else /* if ( node == parent->right ) */ {
						parent->right = right_balance(node);
					}
				} else /* if ( parent == NULL ) */ {
					avl->root = right_balance(node);
				}
				return AVL_OK;
			}
		}

		node = node->parent;
	}

	return AVL_OK;
}
#elif RECURSIVE
static signed char __attribute__((nonnull))
avl_node_insert ( avl_node_t ** const nodep,
                  avl_node_t * const new )
{
	if ( *nodep == NULL ) {
		*nodep = new;
		return 1;
	}

	avl_node_t * const node = *nodep;
	assert(node->factor >= -1 && node->factor <= 1);

	if ( new->key < node->key ) {
		const signed char ret = avl_node_insert(&node->left, new);
		if ( ret == 0 ) {
			return 0;
		} else if ( ret == 1 ) {
			if ( node->factor == -1 ) {
				*nodep = left_balance(*nodep);

				/* Balancing reduces the height of the tree with one.
				 * This reduction of height neutralizes the increase in
				 * height caused by the insertion, thus 0 should be
				 * returned. */
				return 0;
			} else {
				node->factor--;

				/* The factor can now be either -1 or 0. If the factor
				 * is zero the inserted node did not increase the height
				 * of the tree, but if the factor is -1 the increase of
				 * the factor (and the sub-tree height) was caused by
				 * the insertion. */
				return -node->factor; 
			}
		}
	} else if ( new->key > node->key ) {
		const signed char ret = avl_node_insert(&node->right, new);
		if ( ret == 0 ) {
			return 0;
		} else if ( ret == 1 ) {
			if ( node->factor == 1 ) {
				*nodep = right_balance(*nodep);
				return 0;
			} else {
				node->factor++;

				/* The factor can now be either 0 or 1. If the factor
				 * is zero the inserted node did not increase the height
				 * of the tree but if the factor is 1  the increase of
				 * the factor (and the sub-tree height) was caused by
				 * the insertion. */
				return node->factor;
			}
		}
	}

	/* The node already existed. */
	return -1;
}

avl_error_t __attribute__((nonnull))
avl_insert ( avl_t * const avl, const int key, const int value )
{
	avl_node_t * const new = avl_node_new(avl, key, value);
	if ( new == NULL ) {
		return AVL_NOMEM;
	}

	const signed char ret = avl_node_insert(&avl->root, new);
	if ( ret < 0 ) {
		avl_node_remove(avl, new);
		return AVL_EXISTS;
	}

	return AVL_OK;
}
#elif STACK
static int __attribute__((nonnull))
avl_stack_push ( avl_t * const avl, avl_node_t * const node )
{
	if ( avl->stack_len == avl->stack_size ) {
		avl->stack_size++;
		avl->stack = realloc(avl->stack,
		                     avl->stack_size * sizeof(avl_node_t *));
		if ( avl->stack == NULL ) {
			perror("realloc");
			return 1;
		}
	}

	avl->stack[avl->stack_len] = node;
	avl->stack_len++;
	return 0;
}

static avl_node_t * __attribute__((nonnull))
avl_stack_pop ( avl_t * const avl )
{
	if ( avl->stack_len == 0 ) {
		return NULL;
	}

	avl->stack_len--;
	return avl->stack[avl->stack_len];
}

static inline void __attribute__((nonnull))
avl_stack_clear ( avl_t * const avl )
{
	avl->stack_len = 0;
	return;
}

avl_error_t __attribute__((nonnull))
avl_insert ( avl_t * const avl,
             const int key,
             const int value )
{
	if ( avl->root == NULL ) {
		avl->root = avl_node_new(avl, key, value);
		if ( avl->root == NULL ) {
			return AVL_NOMEM;
		}
		return AVL_OK;
	}

	avl_stack_clear(avl);

	avl_node_t * node = avl->root;
	while ( true ) {
		 if ( key < node->key ) {
			if ( node->left != NULL ) {
				if ( avl_stack_push(avl, node) ) {
					return AVL_NOMEM;
				}
				node = node->left;
			} else /* if ( node->left == NULL ) */ {
				avl_node_t * const new = avl_node_new(avl, key, value);
				if ( new == NULL ) {
					return AVL_NOMEM;
				}

				node->left = new;
				break;
			}
		} else if ( key > node->key ) {
			if ( node->right != NULL ) {
				if ( avl_stack_push(avl, node) ) {
					return AVL_NOMEM;
				}
				node = node->right;
			} else /* if ( node->right == NULL ) */ {
				avl_node_t * const new = avl_node_new(avl, key, value);
				if ( new == NULL ) {
					return AVL_NOMEM;
				}

				node->right = new;
				break;
			}
		} else /* if ( key == node->key ) */ {
			return AVL_EXISTS;
		}
	}

	while ( node != NULL ) {
		if ( key < node->key ) {
			node->factor--;
			if ( node->factor == 0 ) {
				return AVL_OK;
			} else if ( node->factor == -2 ) {
				avl_node_t * const parent = avl_stack_pop(avl);
				if ( parent != NULL ) {
					assert(node == parent->left ||
					       node == parent->right);
					if ( node == parent->left ) {
						parent->left = left_balance(node);
					} else /* if ( node == parent->right ) */ {
						parent->right = left_balance(node);
					}
				} else /* if ( parent == NULL ) */ {
					avl->root = left_balance(node);
				}
				return AVL_OK;
			}
		} else /* if ( key > node->key ) */ {
			node->factor++;
			if ( node->factor == 0 ) {
				return AVL_OK;
			} else if ( node->factor == 2 ) {
				avl_node_t * const parent = avl_stack_pop(avl);
				if ( parent != NULL ) {
					assert(node == parent->left ||
					       node == parent->right);
					if ( node == parent->left ) {
						parent->left = right_balance(node);
					} else /* if ( node == parent->right ) */ {
						parent->right = right_balance(node);
					}
				} else /* if ( parent == NULL ) */ {
					avl->root = right_balance(node);
				}
				return AVL_OK;
			}
		}

		node = avl_stack_pop(avl);
	}

	return AVL_OK;
}
#endif

int __attribute__((nonnull))
avl_search ( const avl_t * const avl,
             const int key,
             int * const valuep )
{
	const avl_node_t * node = avl->root;
	while ( node != NULL ) {
		if ( key > node->key ) {
			node = node->right;
		} else if ( key < node->key ) {
			node = node->left;
		} else /* if ( key == node->key ) */ {
			*valuep = node->value;
			return 0;
		}
	}
	return 1;
}

static void __attribute__((nonnull))
avl_sanity_check_fail ( const avl_node_t * const node,
                        const size_t left_height,
                        const size_t right_height,
                        const size_t depth )
{
	fprintf(stderr,
	        "AVL sanity check failed at:\n"
	        "  key:          %d\n"
	        "  value:        %d\n"
	        "  depth:        %lu\n"
	        "  factor:       %d\n",
	        node->key, node->value, depth, node->factor);
	if ( left_height == SIZE_MAX ) {
		fputs("  left_height:  unknown\n", stderr);
	} else {
		fprintf(stderr, "  left_height:  %lu\n", left_height);
	}
	if ( right_height == SIZE_MAX ) {
		fputs("  right_height: unknonw\n", stderr);
	} else {
		fprintf(stderr, "  right_height: %lu\n", right_height);
	}

	if ( node->left == NULL ) {
		fputs("  left node: NULL\n", stderr);
	} else {
		fprintf(stderr,
		        "  left node:\n"
		        "    key:   %d\n"
		        "    value: %d\n",
		        node->left->key, node->left->value);
	}

	if ( node->right == NULL ) {
		fputs("  right node: NULL\n", stderr);
	} else {
		fprintf(stderr,
		        "  right node:\n"
		        "    key:   %d\n"
		        "    value: %d\n",
		        node->right->key, node->right->value);
	}

	return;
}

static int __attribute__((nonnull))
avl_node_check_recursive ( const avl_node_t * const node,
                           const size_t depth,
                           size_t * const heightp )
{
	size_t left_height = SIZE_MAX;
	size_t right_height = SIZE_MAX;

	if ( node->left != NULL ) {
		const avl_node_t * const left = node->left;
		if ( node->key <= left->key ) {
			avl_sanity_check_fail(node, left_height, right_height,
			                      depth);
			return 1;
		}
		if ( avl_node_check_recursive(left, depth + 1, &left_height) ) {
			return 1;
		}
	} else {
		left_height = 0;
	}

	if ( node->right != NULL ) {
		const avl_node_t * const right = node->right;
		if ( node->key >= right->key ) {
			avl_sanity_check_fail(node, left_height, right_height,
			                      depth);
			return 1;
		}
		if ( avl_node_check_recursive(right, depth + 1, &right_height) )
		{
			return 1;
		}
	} else {
		right_height = 0;
	}


	if ( right_height - left_height != node->factor ) {
		avl_sanity_check_fail(node, left_height, right_height, depth);
		return 1;
	} else if ( node->factor < -1 || node->factor > 1 ) {
		avl_sanity_check_fail(node, left_height, right_height, depth);
		return 1;
	}

	if ( left_height >= right_height ) {
		*heightp = left_height + 1;
	} else /* if ( left_height < right_height ) */ {
		*heightp = right_height + 1;
	}

	return 0;
}

int __attribute__((nonnull))
avl_check ( const avl_t * const avl )
{
	if ( avl->root != NULL ) {
		size_t height;
		return avl_node_check_recursive(avl->root, 0, &height);
	} else {
		return 0;
	}
}
