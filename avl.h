/*
 * This file is part of dscomp-avl.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#ifndef AVL_H
# define AVL_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

/* size_t */
# if HAVE_STDDEF_H
#  include <stddef.h>
# endif /* HAVE_STDDEF_H */

/* Define to recycle destroyed nodes to avoid unnecessary calls to
 * malloc(). */
# if !defined(RECYCLE)
#  define RECYCLE 0
# endif /* !defined(RECYCLE) */

/* Select a default implementation if none was requested. */
# if !PARENT && !RECURSIVE && !STACK
#  define RECURSIVE 1
# elif PARENT && RECURSIVE || PARENT && STACK || RECURSIVE && STACK
#  error Only one of PARENT, RECURSIVE and STACK are allowed at once.
# endif

enum avl_error_e {
	AVL_OK = 0,
	AVL_EXISTS,
	AVL_NOMEM
};
typedef enum avl_error_e avl_error_t;

struct avl_node_s {
	int key;
	int value;

	/* The factor of the node is the same as
	 * {height of right sub-tree} - {height of left sub-tree} */
	signed char factor;
	struct avl_node_s * left;
	struct avl_node_s * right;
# if PARENT
	struct avl_node_s * parent;
# endif /* PARENT */
};
typedef struct avl_node_s avl_node_t;

struct avl_s {
	avl_node_t * root;
# if RECYCLE
	avl_node_t * recycled;
# endif /* RECYCLE */

# if STACK
	avl_node_t ** stack;
	size_t stack_len;
	size_t stack_size;
# endif /* STACK */
};
typedef struct avl_s avl_t;

avl_t * avl_new ( void );
void avl_destroy ( avl_t * const avl )
	__attribute__((nonnull));

void avl_clear ( avl_t * const avl )
	__attribute__((nonnull));

avl_error_t avl_insert ( avl_t * const avl,
                         const int key,
                         const int value )
	__attribute__((nonnull));

int avl_search ( const avl_t * const avl,
                 const int key,
                 int * const valuep )
	__attribute__((nonnull));

int avl_check ( const avl_t * const avl ) __attribute__((nonnull));

#endif /* !AVL_H */
